import React from "react";
import SearchBar from "./SearchBar";
import "./App.css";
import TreinDrukte from "./TreinDrukte";

import { Provider } from "react-redux";
import { store } from "./store";

class App extends React.Component {

  onSubmitLog = searchTerm => {
    console.log(searchTerm);
  }

  render() {
    return (
      <div className="container">
        <Provider store={store}>
          <TreinDrukte/>
        </Provider>
      </div>
    )
  }
}

//<h1>Hallo test 1 2 3</h1>
//<SearchBar onSubmit={this.onSubmitLog}/>
export default App;
