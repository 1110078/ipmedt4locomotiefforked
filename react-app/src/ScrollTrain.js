import React from "react";
import ScrollMenu from 'react-horizontal-scrolling-menu';
import "./ScrollTrain.css";

// list of Train parts
const list = [
  { treinDeel: {
    coupé: 1,
    img: '/Img/TreinDeel1.png',
    stoelen : {
      totaal: 110,
      bezet: 14
      }
    }
  },
  { treinDeel: {
    coupé: 2,
    img: '/Img/TreinDeel2.png',
    stoelen : {
      totaal: 93,
      bezet: 29
      }
    }
  },
  { treinDeel: {
    coupé: 3,
    img: '/Img/TreinDeel3.png',
    stoelen : {
      totaal: 92,
      bezet: 83
      }
    }
  },
  { treinDeel: {
    coupé: 4,
    img: '/Img/TreinDeel4.png',
    stoelen : {
      totaal: 110,
      bezet: 51
      }
    }
  },
];

// example values
const drukte = "rustig";

// One item component
// selected prop will be passed
const MenuItem = ({text, selected}) => {
  return <img
    className={`menu-item ${selected ? 'active' : ''}`}
    src={text}
    alt={text.replace('/Img/','')}
    />;
};

// All items component
export const Menu = (list, selected) =>
  list.map(el => {
    const {treinDeel} = el;

    return <MenuItem text={treinDeel.img} key={treinDeel.coupé} selected={selected} />;
  });


const Arrow = ({ text, className }) => {
  return (
    <div
      className={className}
    >{text}</div>
  );
};


const ArrowLeft = Arrow({ text: '<', className: 'arrow-prev' });
const ArrowRight = Arrow({ text: '>', className: 'arrow-next' });

const selected = list[0].treinDeel.coupé;
const stoelenTotaal = list[0].treinDeel.stoelen.totaal;
const stoelenBezet = list[0].treinDeel.stoelen.bezet;

class ScrollTrain extends React.Component {
  constructor(props) {
    super(props);
    // call it again if items count changes
    this.menuItems = Menu(list, selected);
  }

  state = {
    selected,
    stoelenTotaal,
    stoelenBezet
  };

  onSelect = key => {
    this.setState({ selected: key });
    this.setState({ stoelenTotaal: list[key-1].treinDeel.stoelen.totaal });
    this.setState({ stoelenBezet: list[key-1].treinDeel.stoelen.bezet });
  }


  render() {
    const { selected } = this.state;
    // Create menu from items
    const menu = this.menuItems;

    return (
      <div className="ScrollTrain">
        <h1>Het is momenteel: {drukte}</h1>
        <h1>Coupé: {selected}</h1>
        <h1>{this.state.stoelenBezet}/{this.state.stoelenTotaal} Stoelen bezet</h1>

        <ScrollMenu
          data={menu}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          selected={selected}
          onSelect={this.onSelect}
          wheel={false}
        />
      </div>
    );
  }
}

export default ScrollTrain;
