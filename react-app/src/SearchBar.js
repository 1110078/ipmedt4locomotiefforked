import React from "react";
import "./SearchBar.css";
import { connect } from "react-redux";
import { changeSearchTerm } from "./actions";

class SearchBar extends React.Component {

  onSearch = event => {
    this.props.changeSearchTerm(event.target.value);
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.props.searchTerm);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
          className="searchbar"
          type="text" placeholder="Dit is een test searchbar die alles logt wat je stuurt"
          value={this.props.searchTerm}
          onChange={this.onSearch}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { searchTerm: state.searchTerm };
};

export default connect(mapStateToProps,
  {
    changeSearchTerm: changeSearchTerm
  }
)(SearchBar);
