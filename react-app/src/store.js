import { combineReducers, createStore } from "redux";
import { searchTerm } from "./reducers";

export const store = createStore(
  combineReducers({
    searchTerm
  })
);
